require("dotenv").config();

const fs = require("fs");
const https = require("https");
const http = require("http");

let config = loadConfig();
const app = require("./src/app")(config);

async function buildServer(app) {
  console.log("Creating HTTP server");
  return http.createServer({}, app);
}

function loadConfig() {
  let config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
  return config;
}
  
async function run() {
  let server = await buildServer(app);
  server.listen(config.app.port, async function() {
    let host = server.address().address;
    let port = server.address().port;

    console.log("App listening at http://" + host + ":" + port);
    console.log("App startup " + new Date());
  })
}

run();
